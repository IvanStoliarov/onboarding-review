var gulp = require("gulp");
var sass = require("gulp-sass");
// var rigger = require("gulp-rigger");
var autoprefixer = require("gulp-autoprefixer");
var browserSync = require("browser-sync").create();
var cssmin = require("gulp-cssmin");
var minify = require("gulp-minify");
var imagemin = require("gulp-imagemin");
var image = require('gulp-image');

// RIGGER
// gulp.task("rigger", function() {
// 	gulp.src("./include/*.html")
// 		.pipe(rigger())
// 		.pipe(gulp.dest("./"));
// });

// SASS
gulp.task("sass", function() {
	return gulp
		.src("./sass/**/*")
		// .pipe(sass().on("error", sass.logError))
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(autoprefixer({ remove: false, grid: true, browsers: ["ie >= 11", "last 2 versions"] }))
		.pipe(gulp.dest("./css"));
});

// gulp.task("sass:watch", function() {
// 	gulp.watch("./sass/**/*.scss", ["sass"]);
// });

// BROWSERSYNC
gulp.task("browserSync", function() {
	browserSync.init({
		server: {
			baseDir: "./"
		}
	});
});

// WATCH
gulp.task("watch", ["browserSync", "sass"], function() {
	// gulp.watch("./include/*.html", ["rigger"]);
	gulp.watch("./css/*.css", browserSync.reload);
	gulp.watch("./*.html", browserSync.reload);
	gulp.watch("./js/**/*.js", browserSync.reload);
	gulp.watch("./sass/**/*.scss", ["sass"]);
});

// MIN CSS
// var rename = require('gulp-rename');

gulp.task("minCss", function() {
	gulp.src("./css/*.css")
		.pipe(cssmin())
		// .pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest("../dist/css"));
});

// MIN JS
gulp.task("minJs", function() {
	gulp.src("./js/*.js")
		.pipe(
			minify({
				ext: {
					src: "-debug.js",
					min: ".js"
				}
			})
		)
		.pipe(gulp.dest("../dist/js"));
});

// COMPRESS IMAGES
gulp.task("compressImg", function() {
	gulp.src("img/**/*")
		.pipe(imagemin())
		.pipe(gulp.dest("../dist/img"));
});

gulp.task("image", function() {
	gulp.src("img/**/*")
		.pipe(imagemin())
		.pipe(gulp.dest("../dist/img"));
});

// MOVE FILES
gulp.task("move:images", function() {
	return gulp.src("./img/**/*").pipe(gulp.dest("../dist/img"));
});

gulp.task("move:fonts", function() {
	return gulp.src("./fonts/**/*").pipe(gulp.dest("../dist/fonts"));
});

gulp.task("move:html", function() {
	return gulp.src("./*.html").pipe(gulp.dest("../dist/"));
});

gulp.task("move:css", function() {
	return gulp.src("./css/**/*").pipe(gulp.dest("../dist/css"));
});

// BUILD
gulp.task("build", [ `move:fonts`, "move:html", `move:css`, "minJs"], function() {
	console.log("Building files");
});
