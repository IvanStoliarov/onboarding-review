// Make task active
(function() {
  let task = document.querySelectorAll(".task");

  if (task.length > 0) {
    for (let i = 0; i < task.length; i++) {
      let element = task[i];

      element.addEventListener("click", function() {
        for (let i = 0; i < task.length; i++) {
          let element = task[i];
          element.classList.remove("task_active");
        }
        this.classList.add("task_active");
      });
    }
  }
})();

